import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

public class Library {
	ArrayList<Book> bookList = new ArrayList<Book>();
        
	public ArrayList<Book> SortByBasePrice(ArrayList<Book> books){
		   boolean sorted = false;
		   // sort the array		   
	        	 for (int i=books.size()-1; i >= 0; i--) {
		        	 for (int j=0; j < i; j++) {
		           if (books.get(j).getBasePrice() < books.get(j+1).getBasePrice()) {
		        		   Collections.swap(books, j, j+1);
		           }  
	        	 }
	       }
	       
	       return books;
	       }

}