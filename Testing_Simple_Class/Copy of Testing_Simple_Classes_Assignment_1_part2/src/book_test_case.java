import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class book_test_case {
	Book book;
	
	@Before
	public void setUp(){
		book = new Book(100.0, 20.0, 10.0, true, false);
	}
	
	@Test
	public void test_instantiate_book_without_info() {
		Book bookTest = new Book();
		assertEquals(bookTest.getBasePrice(), 0, 7);
		assertEquals(bookTest.getVat(), 0, 7);
		assertEquals(bookTest.getDiscount(), 0, 7);
		assertFalse(bookTest.getBestSeller());
	}

	@Test
	public void test_positive_setBasePrice() {
		book.setBasePrice(148.0);
		assertEquals(148.0, book.getBasePrice(), 7);
	}
	
	@Test 
	public void test_book_price(){
		assertTrue(book.getBasePrice() >= 0);
	}
	
	// p.4 and p. 13
	@Test
	public void test_negative_setVat() {
		book.setVat(-18.0);
		assertEquals(book.getVat(), 0, 5);
	}
	
	@Test
	public void test_positive_setVat() {
		book.setVat(18.0);
		assertEquals(book.getVat(), 18.0, 5);
	}
	
	@Test
	public void test_setDiscount() {
		book.setDiscount(28.0);
		assertEquals(28.0, book.getDiscount(), 7);
	}
	
	@Test
	public void test_negative_setDiscount() {
		book.setDiscount(-28.0);
		assertEquals(book.getDiscount(), 0, 5);
	}
	
	@Test
	public void test_more_than_50_setDiscount() {
		book.setDiscount(55.0);
		assertEquals(book.getDiscount(), 50, 5);
	}
	
	// p. 8
	@Test
	public void test_negative_setSellPrice() {
		book.setBasePrice(-1228.0);
		assertFalse(book.setSellPrice());
	}
	
	@Test
	public void test_bestseller_setSellPrice() {
		// p. 10 & p. 8
		book.setBasePrice(100.0);
		book.setVat(10.0);
		book.setDiscount(10.0);
		book.setBestSeller();
		book.setSellPrice();
		assertEquals(book.getSellPrice(), 105.0, 5);
		assertTrue(book.setSellPrice());
	}
	
	@Test
	public void test_positive_setSellPrice() {
		book.setBasePrice(100.0);
		book.resetBestSeller();
		book.setVat(10.0);
		book.setDiscount(10.0);
		book.setSellPrice();
		
		assertEquals(book.getSellPrice(), 100.0, 5);
		assertTrue(book.setSellPrice());
	}
	
	
	@Test 
	public void test_final_sell_price(){
		assertTrue(book.getSellPrice() > 0);
	}
	
	@Test 
	public void test_setBestSeller(){
		assertTrue(book.getBestSeller());
	}
	
	// p.12 
	@Test
	public void test_negative_setBasePrice() {
		book.setBasePrice(-148.0);
		assertEquals(0, book.getBasePrice(), 7);
	}
	
	//-- begin -- updated -- part 2 //
	
	// p. 16
	@Test
	public void test_sale_setSellPrice() {
		book.setBasePrice(100.0);
		book.setVat(10.0);
		book.resetBestSeller();
		book.setOnSale();
		book.setSellPrice();
		assertEquals(book.getSellPrice(), 50.0, 5);
	}
	
	// verify set on sale & discount placed automatically
	@Test 
	public void test_not_bestseller_setOnSale(){
		book.resetBestSeller();
		book.setOnSale();
		assertTrue(book.getOnSale());
		assertEquals(book.getDiscount(), 60.0, 5);
	}
	
	// p. 17
	// set on sale for a bestseller
	@Test 
	public void test_best_seller_setOnSale(){
		book.setBestSeller();
		book.setOnSale();
		assertFalse(book.getOnSale());
		assertTrue(book.getBestSeller());
	}
	
	@Test 
	public void test_on_sale_setBestSeller(){
		// need to reset best seller because the book instance was initiated with seller - true
		book.resetBestSeller();
		book.setOnSale();
		book.setBestSeller();
		assertFalse(book.getBestSeller());
		assertTrue(book.getOnSale());
	}
	
	//-- end -- updated -- part 2 //
	
	
}
