public class Book {
	private double basePrice = 0;
	private double vat = 0;
	private double discount = 0;
	private double sellPrice;
	private boolean bestseller = false;

	public Book() {
		this.basePrice = 0;
		this.vat = 0;
		this.discount = 0;
		this.bestseller = false;
	} 

	public Book(double basePrice, double discount, double vat, boolean bestseller) {
		this.basePrice = basePrice;
		this.discount = discount;
		this.vat = vat;
		this.bestseller = bestseller;
		setSellPrice();
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		if (basePrice < 0){
			this.basePrice = 0;
		}
		else {
			this.basePrice = basePrice;
		}
		setSellPrice();
	}

	public double getVat() {
		return this.vat;
	}

	public void setVat(double vat) {
		if (vat < 0){
			this.vat = 0;
		}
		else {
			this.vat = vat;
		}
		
		setSellPrice();
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		if (discount < 0){
			this.discount = 0;
		}
		
		else if (discount > 50){
			System.out.println("discount can not be larger than 50 %");
			this.discount = 50;
		} 
		else {
			this.discount = discount;
		}
		setSellPrice();
	}
	
	public boolean getBestSeller(){
		return this.bestseller; 
	}
	
	public void resetBestSeller(){
		this.bestseller = false;
		setSellPrice();
	}
	
	public void setBestSeller(){
		this.bestseller = true;
		setSellPrice();
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public boolean setSellPrice() {
		if(this.basePrice > 0){
			if(this.bestseller == true){
				this.sellPrice = this.basePrice * ((100 + this.vat - this.discount * 0.5) / 100);
				return true;
			}
			else{
				this.sellPrice = this.basePrice * ((100 + this.vat - this.discount) / 100);
				return true;
			}
		}
		else
		{
			System.out.println("The base price must be greater than zero"); 
			return false;
		}
	}

}