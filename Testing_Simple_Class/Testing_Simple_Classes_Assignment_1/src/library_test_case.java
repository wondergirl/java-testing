import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class library_test_case {
	ArrayList<Book> books = new ArrayList<Book>();
	Book book1, book2, book3;
	Library booksList = new Library();
	ArrayList<Book> result = new ArrayList<Book>();
	
	@Before
	public void setUp(){
		book1 = new Book(100.0, 20.0, 10.0, true);
		book2 = new Book(90.0, 20.0, 10.0, true);
		book3 = new Book(110.0, 20.0, 10.0, true);
		books.add(book1);
		books.add(book2);
		books.add(book3);
		
		result.add(book3);
		result.add(book1);
		result.add(book2);
	}

	@Test
	public void test_SortByBasePrice() {
		/*for(int i = 0; i < result.size(); i++) {
			Book ite = result.get(i);
			System.out.print((ite.getBasePrice()));
		} */
		assertEquals(booksList.SortByBasePrice(books), result);
	}
	
	
	
}
