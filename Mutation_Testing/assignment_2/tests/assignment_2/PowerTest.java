package assignment_2;

import junit.framework.TestCase;

public class PowerTest extends TestCase {
	
	 public void testRightPower(){
		 	assertEquals(Power.power(2,3), 8);
	 }
	 
	 public void testZeroPower(){
			assertEquals(Power.power(2, 0), 1);
	 }
	 
	 public void testZeroPower2(){
			assertEquals(Power.power(0, 3), 0);
	 }
	 
	 public void testPower(){
			assertEquals(Power.power(2, 2), 4);
	 }
	 
	 public void testNegativePower(){
			assertEquals(Power.power(-2, 3), -8);	
	 }
	 
	 public void testOnePower(){
			assertEquals(Power.power(2, 1), 2);
	 }
	 
	 public void testNegativePower2(){
			assertEquals(Power.power(2, -3), 2);
	 }
	 	 

}
